package com.ridepal.controllers;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.stereotype.Controller;
import com.ridepal.services.common.*;
import org.springframework.ui.Model;
import com.ridepal.models.*;

@Controller
public class AlbumsController {

    private AlbumsService albumsService;
    private TracksService tracksService;
    private GenresService genresService;

    public AlbumsController(AlbumsService albumsService,
                            TracksService tracksService,
                            GenresService genresService) {
        this.albumsService = albumsService;
        this.tracksService = tracksService;
        this.genresService = genresService;

    }

    @GetMapping("/albums")
    public String showAllAlbums(Model model) {
        model.addAttribute("albums", albumsService.getAllAlbums());
        return "albums_all_in_db";
    }


    @GetMapping("/albums/{id}")
    public String showAlbumDetails(@PathVariable int id, Model model) {
        Album album = albumsService.getAlbumByID(id);
        Genre genre = genresService.getGenreByDeezerID(album.getGenreID());
        album.setGenre(genre);
        for (Track track : album.getTracks()) {
            track.setPlaytime(tracksService.formatTotalPlaytime(track.getDuration()));
        }
        model.addAttribute("album", albumsService.getAlbumByID(id));
        return "album_details";
    }
}
