package com.ridepal.controllers;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import com.ridepal.exceptions.DuplicateEntityException;
import org.springframework.web.multipart.MultipartFile;
import com.ridepal.exceptions.EntityNotFoundException;
import com.ridepal.services.common.PlaylistsService;
import org.springframework.validation.BindingResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.ridepal.services.common.UsersService;
import com.ridepal.models.dtoes.UserProfileDto;
import org.springframework.http.HttpStatus;
import com.ridepal.models.dtoes.DtoMapper;
import org.springframework.ui.Model;
import com.ridepal.models.Playlist;
import com.ridepal.utils.Helper;
import com.ridepal.models.User;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Set;

@Controller
public class UsersController {
    private PlaylistsService playlistsService;
    private UsersService usersService;
    private DtoMapper mapper;

    @Autowired
    public UsersController(PlaylistsService playlistsService,
                           UsersService usersService,
                           DtoMapper mapper) {
        this.playlistsService = playlistsService;
        this.usersService = usersService;
        this.mapper = mapper;
    }

    @GetMapping("/users/{username}")
    @PreAuthorize("authentication.principal.username == #username or hasAuthority('ADMIN')")
    public String showUserProfile(@PathVariable String username,
                                  Model model) {
        Authentication authentication = SecurityContextHolder
                                       .getContext()
                                       .getAuthentication();
        String loggedUser = authentication.getName();

        try {
            User user = usersService.getUsersWithExactUsername(username).get(0);
//            Set<Playlist> playlists = user.getPlaylists();
            List<Playlist> playlists =  playlistsService.getPlaylistsByCreator(user.getID());

            for (Playlist playlist : playlists) {
                playlist.setPlaytime(playlistsService.formatTotalPlaytime(playlist.getTotalPlaytime()));
            }
            model.addAttribute("loggedUser", loggedUser);
            model.addAttribute("playlists", playlists);
            model.addAttribute("user", user);

            return "user-profile";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/users/{username}/update")
    @PreAuthorize("authentication.principal.username == #username or hasAuthority('ADMIN')")
    public String showUpdateUserForm(@PathVariable String username,
                                     Model model,
                                     UserProfileDto userProfileDto) {
        model.addAttribute("userProfileDto", userProfileDto);

        try {
            User user = usersService.getUsersWithExactUsername(username).get(0);
            model.addAttribute("user", user);
            return "user-update";

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/users/{username}/update")
    public String updateUser(@PathVariable String username,
                             @RequestParam (value = "file") MultipartFile file,
                             @Valid @ModelAttribute("userProfileDto") UserProfileDto userProfileDto,
                             BindingResult bindingResult,
                             Model model) {
        String error = "";
        model.addAttribute("userProfileDto", userProfileDto);

        if (bindingResult.hasErrors()) {
            error = Helper.getErrorMessage(bindingResult, error);
            model.addAttribute("error", error);

            return "user-update";
        }
        try {
            User update = mapper.fromUserProfileDto(userProfileDto);

            usersService.updateUser(username, update, file);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());

        } catch (DuplicateEntityException de) {
            User userToUpdate = usersService.getUsersWithExactUsername(username).get(0);
            model.addAttribute("file", file);
            model.addAttribute("user", userToUpdate);
            model.addAttribute("username", username);
            model.addAttribute("error", de.getMessage());
            model.addAttribute("userProfileDto", userProfileDto);
            return "user-update";
        }
        return "redirect:/";
    }

    @PostMapping("/users/{username}/delete")
    public String deleteUser(@PathVariable String username) {
        Authentication authentication = SecurityContextHolder
                                       .getContext()
                                       .getAuthentication();
        usersService.deleteUser(username);
        if(AuthorityUtils.authorityListToSet(authentication.getAuthorities()).size() != 2){
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        return "redirect:/";
    }

    @GetMapping("/users")
    @PreAuthorize("hasAuthority('ADMIN')")
    public String getAllUsers(Model model){
        try{
            List<User> allUsers = usersService.getAllUsers();
            model.addAttribute("allUsers", allUsers);
            return "users_all_admin";

        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}