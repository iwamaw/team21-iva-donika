package com.ridepal.controllers;

import com.ridepal.models.dtoes.DtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import com.ridepal.exceptions.BingMapsConnectionException;
import org.springframework.web.bind.annotation.GetMapping;
import com.ridepal.models.dtoes.GeneratePlaylistAlgDTO;
import com.ridepal.services.common.PlaylistsService;
import com.ridepal.exceptions.DataNotFoundException;
import org.springframework.stereotype.Controller;
import com.ridepal.services.common.GenresService;
import com.ridepal.services.common.UsersService;
import com.ridepal.models.GenrePercentage;
import org.springframework.ui.Model;
import com.ridepal.models.User;

import java.security.Principal;
import javax.validation.Valid;
import java.util.List;

import static com.ridepal.utils.Constants.EMPTY_LIST_OF_GENRES;
import static com.ridepal.utils.Constants.FAILED_BING_MAPS_CONNECTION_MESSAGE;

@Controller
public class GenerateAlgorithmController {
    private PlaylistsService playlistsService;
    private GenresService genresService;
    private UsersService usersService;
    private DtoMapper mapper;

    @Autowired
    public GenerateAlgorithmController(PlaylistsService playlistsService,
                                       GenresService genresService,
                                       UsersService usersService,
                                       DtoMapper mapper) {
        this.playlistsService = playlistsService;
        this.genresService = genresService;
        this.usersService = usersService;
        this.mapper = mapper;
    }

    @GetMapping("/generate")
    public String showGeneratePlaylistForm(Model model) {
        model.addAttribute("dto", new GeneratePlaylistAlgDTO());
        model.addAttribute("genres", genresService.getAllGenres());
        return "generate_algorithm";
    }

    @PostMapping("/generate")
    public String generatePlaylist(Principal principal,
                                   @Valid @ModelAttribute GeneratePlaylistAlgDTO dto,
                                   Model model) {

        try {
            List<GenrePercentage> genreForAlg = mapper.fromGenrePercentageDTO(dto);
            String username = principal.getName();
            User user = usersService.getUsersWithExactUsername(username).get(0);
            model.addAttribute("username", username);
            model.addAttribute("dto", dto);
            model.addAttribute("genres", genresService.getAllGenres());
            playlistsService.generatePlaylist(
                    dto.getStartLocation(),
                    dto.getEndLocation(),
                    genreForAlg,
                    user,
                    dto.getPlaylistName(),
                    dto.isPublic(),
                    dto.isRepeatsArtists(),
                    dto.usesTopRanked());
            return String.format("redirect:/users/%s", username);

        } catch (BingMapsConnectionException b) {
            model.addAttribute("error", FAILED_BING_MAPS_CONNECTION_MESSAGE);
            return "generate_algorithm";

        } catch (DataNotFoundException d) {
            model.addAttribute("error", EMPTY_LIST_OF_GENRES);
            model.addAttribute("dto", new GeneratePlaylistAlgDTO());
            model.addAttribute("genres", genresService.getAllGenres());

            return "generate_algorithm";
        }
    }
}
