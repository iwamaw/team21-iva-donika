package com.ridepal.models;

import javax.persistence.*;

@Entity
@Table(name ="gender")
public class Gender {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Gender_idx")
    private int id;

    @Column(name = "Name")
    private String name;

    public Gender(){}

    public Gender(String name){
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
