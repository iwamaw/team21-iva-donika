package com.ridepal.models.dtoes;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class GeneratePlaylistAlgDTO {
    @NotBlank
    private String startLocation;
    @NotBlank
    private String endLocation;
    @NotBlank
    private String playlistName;
    @NotNull
    private List<GenrePercentageDTO> pickGenres;

    private boolean repeatsArtists;

    private boolean usesTopRanked;

    private boolean Public;

    public boolean isPublic() {
        return Public;
    }

    public void setPublic(boolean aPublic) {
        this.Public = aPublic;
    }

    public GeneratePlaylistAlgDTO() {
        pickGenres = new ArrayList<>();
    }

    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(String startLocation) {
        this.startLocation = startLocation;
    }

    public String getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(String endLocation) {
        this.endLocation = endLocation;
    }

    public String getPlaylistName() {
        return playlistName;
    }

    public void setPlaylistName(String playlistName) {
        this.playlistName = playlistName;
    }

    public List<GenrePercentageDTO> getPickGenres() {
        return pickGenres;
    }

    public void setPickGenres(List<GenrePercentageDTO> pickGenres) {
        this.pickGenres = pickGenres;
    }

    public boolean usesTopRanked() {
        return usesTopRanked;
    }

    public void setUsesTopRanked(boolean usesTopRanked) {
        this.usesTopRanked = usesTopRanked;
    }

    public boolean isRepeatsArtists() {
        return repeatsArtists;
    }

    public void setRepeatsArtists(boolean repeatsArtists) {
        this.repeatsArtists = repeatsArtists;
    }

    public boolean isUsesTopRanked() {
        return usesTopRanked;
    }
}
