package com.ridepal.models.dtoes;

import org.hibernate.validator.constraints.ScriptAssert;
import javax.validation.constraints.NotBlank;
import static com.ridepal.utils.Constants.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ScriptAssert(lang = "javascript", alias = "_",
        script = "_.passwordConfirmation != null && _.passwordConfirmation.equals(_.password)")
public class UserRegDto {
    @NotNull
    @NotBlank(message = REQUIRED_USERNAME_MESSAGE)
    @Size(min = MINIMUM_SIZE_FOR_USERNAME, max = MAXIMUM_SIZE_FOR_USERNAME, message = UNSUITABLE_USERNAME_MASSAGE)
    private String username;

    @NotNull
    @NotBlank(message = REQUIRED_PASSWORD_MESSAGE)
    @Size(min = MINIMUM_SIZE_FOR_PASSWORD, max = MAXIMUM_SIZE_FOR_PASSWORD, message = UNSUITABLE_PASSWORD_MASSAGE)
    private String password;

    @NotNull
    @NotBlank(message = REQUIRED_PASSWORD_CONFIRM_MESSAGE)
    private String passwordConfirmation;

    public UserRegDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
