package com.ridepal.repositories.common;

import com.ridepal.models.Artist;
import java.util.List;

public interface ArtistsRepository {
    List<Artist> getAllArtists();

    List<Artist> getArtistByName(String name);

    Artist getArtistByID(int id);

    List<Artist> getArtistByDeezerID(int deezerId);

    Artist createArtist(String name, int deezerId, String tracklist, int genreId, String picture);
}
