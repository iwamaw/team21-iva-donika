package com.ridepal.repositories.common;

import org.springframework.web.multipart.MultipartFile;
import com.ridepal.models.User;
import java.io.IOException;
import java.util.List;

public interface UsersRepository {
    List<User> getAllUsers();

    List<User> getUsersWithCommonUsername(String name);

    List<User> getUserByID(int id);

    void createUser(User userToCreate);

    void deleteUser(String username);

    void updateUser(String username, User update);

    void updateUser(String username, User update, MultipartFile file) throws IOException;

    List<User> getUserWithExactUsername(String username);

    List<User> getUserByEmail(String email);

    boolean isUserAuthorized(String username, String authority);
}
