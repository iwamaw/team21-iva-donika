package com.ridepal.repositories.common;

import com.ridepal.models.Track;
import java.util.List;

public interface TracksRepository {
    List<Track> getAllTracks();

    List<Track> getTrackByName(String title);

    Track getTrackById(int id);

    Track createTrack(String title, String link, int duration, int deezerId, int artistId, int albumId, int rank, String previewURL);

    List<Track> getTracksByGenre(int genreID);
}
