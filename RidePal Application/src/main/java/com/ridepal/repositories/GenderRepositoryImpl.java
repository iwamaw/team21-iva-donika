package com.ridepal.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import com.ridepal.repositories.common.GendersRepository;
import org.springframework.stereotype.Repository;
import org.hibernate.SessionFactory;
import com.ridepal.models.Gender;
import org.hibernate.query.Query;
import org.hibernate.Session;

import java.util.List;

@Repository
public class GenderRepositoryImpl implements GendersRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public GenderRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Gender> getAllGenders() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Gender", Gender.class).list();
    }

    @Override
    public Gender getGenderByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Gender> query = session.createQuery(
                "from Gender where name like :name", Gender.class);
        query.setParameter("name", name);
        Gender gender = query.list().get(0);
        return gender;
    }

    @Override
    public Gender getGenderByID(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Gender.class, id);
    }
}

