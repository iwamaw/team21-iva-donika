package com.ridepal.services;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import static com.ridepal.utils.Helper.checkIfUserExists;
import org.springframework.web.multipart.MultipartFile;
import com.ridepal.repositories.common.UsersRepository;
import com.ridepal.exceptions.DuplicateEntityException;
import com.ridepal.exceptions.EntityNotFoundException;
import static com.ridepal.utils.Helper.ifNotExist;
import com.ridepal.services.common.GenderService;
import com.ridepal.services.common.UsersService;
import org.springframework.stereotype.Service;
import static com.ridepal.utils.Constants.*;
import com.ridepal.models.Playlist;
import com.ridepal.models.User;
import java.io.IOException;
import java.util.*;

@Service
public class UsersServiceImpl implements UsersService {
    private UserDetailsManager detailsManager;
    private UsersRepository usersRepository;
    private GenderService genderService;
    private PasswordEncoder encoder;

    @Autowired
    public UsersServiceImpl(@Qualifier("userDetailsManager") UserDetailsManager detailsManager,
                            UsersRepository usersRepository,
                            GenderService genderService,
                            PasswordEncoder encoder) {
        this.detailsManager = detailsManager;
        this.usersRepository = usersRepository;
        this.genderService = genderService;
        this.encoder = encoder;
    }

    @Override
    public List<User> getAllUsers() {
        return usersRepository.getAllUsers();
    }

    @Override
    public List<User> getUsersWithCommonUsername(String username) {
        List<User> users = usersRepository.getUsersWithCommonUsername(username);
        ifNotExist("username", username, users.size() != 0);
        return users;
    }

    public List<User> getUserForSearchMethod(String username) {
        return usersRepository.getUsersWithCommonUsername(username);
    }

    @Override
    public List<User> getUsersWithExactUsername(String username) {
        return usersRepository.getUserWithExactUsername(username);
    }

    @Override
    public List<User> getUserByID(int id) {
        ifNotExist("id", Integer.toString(id), usersRepository.getUserByID(id).size() != 0);
        return usersRepository.getUserByID(id);
    }

    @Override
    public void createUser(User userToCreate) {
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User
                        (userToCreate.getUsername(),
                                encoder.encode(userToCreate.getPassword()),
                                authorities);

        detailsManager.createUser(newUser);
        User userSetGender = getUsersWithCommonUsername(newUser.getUsername()).get(0);
        userSetGender.setGender(genderService.getGenderByName(GENDER_NOT_APPLICABLE));
        usersRepository.updateUser(userSetGender.getUsername(), userSetGender);
    }

    @Override
    public void deleteUser(String username) {
        ifNotExist("username", username,
                checkIfUserExists(usersRepository.getUserWithExactUsername(username)));

        User user = usersRepository.getUserWithExactUsername(username).get(0);
        Set<Playlist> usersPlaylists = user.getPlaylists();
        for (Playlist playlist : usersPlaylists) {
            playlist.setEnabled(false);
        }
        usersRepository.deleteUser(username);
    }

    @Override
    public void updateUser(String username,
                           User update,
                           MultipartFile file) throws IOException {
        ifNotExist("username", username,
                checkIfUserExists(usersRepository.getUserWithExactUsername(username)));

        User userToUpdate = usersRepository.getUserWithExactUsername(username).get(0);

        if (update.getEmail() != null) {
            if (userToUpdate.getEmail() == null) {
                if(checkIfUserExists(usersRepository.getUserByEmail(update.getEmail()))){
                    throw new DuplicateEntityException(
                        String.format(ALREADY_EXISTING_EMAIL_MESSAGE, update.getEmail()));
                }
            } else if(!userToUpdate.getEmail().equals(update.getEmail())){
                checkIfEmailIsTaken(update.getEmail());
                }
            }

        update.setPassword(encoder.encode(update.getPassword()));
        usersRepository.updateUser(username, update, file);
    }

    @Override
    public void updateUser(String username, User update) {
        ifNotExist("username", username,
                checkIfUserExists(usersRepository.getUserWithExactUsername(username)));

        User userToUpdate = usersRepository.getUserWithExactUsername(username).get(0);

        if (userToUpdate.getEmail() != null && !userToUpdate.getEmail().equals(update.getEmail())) {
            checkIfEmailIsTaken(update.getEmail());
        }
        usersRepository.updateUser(username, update);
    }

    public boolean isUserAuthorized(String username, String authority) {
        User user = usersRepository.getUserWithExactUsername(username).get(0);
        if (user == null) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_USER_BY_USERNAME_MESSAGE, username));
        }
        return usersRepository.isUserAuthorized(username, authority);
    }

    private void checkIfEmailIsTaken(String email) {
        if (usersRepository.getUserByEmail(email).size() != 0) {
            throw new DuplicateEntityException(
                    String.format(ALREADY_EXISTING_EMAIL_MESSAGE, email));
        }
    }
}
