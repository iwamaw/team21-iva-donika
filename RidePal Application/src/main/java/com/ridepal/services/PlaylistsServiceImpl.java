package com.ridepal.services;

import org.springframework.beans.factory.annotation.Autowired;
import com.ridepal.repositories.common.PlaylistsRepository;
import org.springframework.web.multipart.MultipartFile;
import com.ridepal.exceptions.EntityNotFoundException;
import com.ridepal.services.common.PlaylistsService;
import com.ridepal.services.common.TracksService;
import com.ridepal.services.common.UsersService;
import com.ridepal.models.dtoes.PlaylistMinDto;
import org.springframework.stereotype.Service;
import static com.ridepal.utils.Constants.*;
import static com.ridepal.utils.Helper.*;

import java.util.stream.Collectors;
import org.omg.CORBA.BAD_PARAM;
import com.ridepal.models.*;
import java.io.IOException;
import java.util.*;

@Service
public class PlaylistsServiceImpl implements PlaylistsService {
    private PlaylistsRepository playlistsRepository;
    private UsersService usersService;
    private TracksService tracksService;

    @Autowired
    public PlaylistsServiceImpl(PlaylistsRepository playlistsRepository,
                                TracksService tracksService,
                                UsersService usersService) {
        this.playlistsRepository = playlistsRepository;
        this.tracksService = tracksService;
        this.usersService = usersService;
    }

    @Override
    public List<PlaylistMinDto> getAllPlaylists() {
        List<com.ridepal.models.Playlist> playlists =
                playlistsRepository.getAllPlaylists();
        formatPlaylistFieldsForView(playlists);
        return fillListOfPlaylistsIntoDtos(playlists);
    }

    @Override
    public Playlist getPlaylistById(int id) {
        Playlist playlist = playlistsRepository.getPlaylistById(id);
        if (playlist == null) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_PLAYLIST_ID_MESSAGE, id));
        }
        return playlist;
    }

    public Playlist getPrivatePlaylistByName(String username, int playlistID) {
        Playlist playlist = playlistsRepository.getPrivatePlaylistByName(username, playlistID);
        if (playlist == null) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_PLAYLIST_ID_MESSAGE, playlistID));
        }
        return playlist;
    }

    public Playlist getPrivatePlaylistByName(String playlistName) {
        Playlist playlist = playlistsRepository.getPrivatePlaylistByName(playlistName);
        if (playlist == null) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_PLAYLIST_MESSAGE, playlistName));
        }
        return playlist;
    }

    @Override
    public List<Playlist> getPlaylistByName(String title) {
        List<Playlist> playlists = playlistsRepository.getPlaylistByName(title);
        if (playlists.size() == 0) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_PLAYLIST_MESSAGE, title));
        }
        return playlists;
    }

    public List<Playlist> getPlaylistsByCreator(int userID) {
        User user = usersService.getUserByID(userID).get(0);
        return playlistsRepository.getPlaylistByCreator(userID);
    }

    @Override
    public Playlist updatePlaylist(int id, Playlist playlist) {
        return playlistsRepository.updatePlaylist(id, playlist);
    }

    @Override
    public Playlist updatePlaylist(int id, Playlist playlist, MultipartFile file) throws IOException {
        return playlistsRepository.updatePlaylist(id, playlist, file);
    }

    @Override
    public Playlist deletePlaylist(int id) {
        return playlistsRepository.deletePlaylist(id);
    }

    @Override
    public Playlist createPlaylist(String title, int totalPlaytime, int creatorID, Set<Track> tracks, Set<Genre> genres,
                                   boolean isPublic) {
        User user = usersService.getUserByID(creatorID).get(0);
        if (user == null) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_USER_BY_ID_MESSAGE, creatorID));
        }
        return playlistsRepository.createPlaylist(title, totalPlaytime, creatorID, tracks, genres, isPublic);
    }

    public void generatePlaylist(String startPoint, String endPoint, List<GenrePercentage> allGenres,
                                 User creator, String title, boolean isPublic, boolean repeatArtists, boolean useTopRanked) {

        int travelDuration=getDurationFromBingMaps(startPoint,endPoint);
        Set<Track> newPlaylist = new HashSet<>();
        Track track;
        int playlistDuration = 0;

        for (GenrePercentage genrePercentage : allGenres) {

            int i = 0;
            Genre genre = genrePercentage.getGenre();
            double percentage = genrePercentage.getPercentage().doubleValue();
            int percentageDuration = (int) Math.round(travelDuration * (percentage / 100));
            List<Track> allTrack=getTracksFromGenreSortedByRank(genre);

            boolean canAddTracks = true;
            Random randomGenerator = new Random();

            while (canAddTracks) {

                if (useTopRanked) {
                    if (allTrack.size() - 1 <= i) {
                        canAddTracks = false;
                        continue;
                    }
                    track = allTrack.get(i);
                } else {
                    track = allTrack.get(randomGenerator.nextInt(allTrack.size() - 1));
                }

                if (!repeatArtists) {
                    Track finalTrack = track;
                    allTrack.removeIf(tr -> tr.getArtist().equals(finalTrack.getArtist()));
                }
                if (percentageDuration - track.getDuration() >= -300) {
                    newPlaylist.add(track);
                    playlistDuration += track.getDuration();
                    percentageDuration -= track.getDuration();
                }
                if (percentageDuration <= 300) {
                    canAddTracks = false;
                }
                if (allTrack.size() <= 1) {
                    canAddTracks = false;
                }
                i++;
            }
        }

        Set<Genre> genresName = getGenresFromGenrePercentageList(allGenres);

        Playlist generatedPlaylist =
                createPlaylist(title, playlistDuration, creator.getID(), newPlaylist, genresName, isPublic);

        int playlistAverageRank = calculatePlaylistAverageRank(generatedPlaylist);
        generatedPlaylist.setRank(playlistAverageRank);
        generatedPlaylist.setTracks(newPlaylist);
        creator.addPlaylist(generatedPlaylist);

        usersService.updateUser(creator.getUsername(), creator);
    }

    @Override
    public String formatTotalPlaytime(int totalPlaytime) {
        int minutes = (totalPlaytime % 3600) / 60;
        int hours = totalPlaytime / 3600;

        return String.format("%02d h : %02d m", hours, minutes);
    }

    @Override
    public String getNumberOfTracks(Playlist playlist) {
        return String.format("%d songs", playlist.getTracks().size());
    }

    @Override
    public List<PlaylistMinDto> filter(String criteria, String... value) {
        List<Playlist> playlists;
        if (criteria.isEmpty()) return getAllPlaylists();

        switch (criteria.toLowerCase()) {
            case "duration":
                playlists = formatPlaylistFieldsForView(
                        playlistsRepository.filterByDuration(Integer.parseInt(value[0]),
                                Integer.parseInt(value[1])));
                return fillListOfPlaylistsIntoDtos(playlists);
            case "creator":
                playlists = formatPlaylistFieldsForView(
                        playlistsRepository.filterByCreator(value[0]));
                return fillListOfPlaylistsIntoDtos(playlists);
            case "genre":
                playlists = formatPlaylistFieldsForView(
                        playlistsRepository.filterByGenre(value[0]));
                return fillListOfPlaylistsIntoDtos(playlists);
            default:
                throw new BAD_PARAM(String.format(INVALID_SORTING_CRITERIA, criteria));
        }
    }

    public List<PlaylistMinDto> sort(String orderBy) {
        List<Playlist> playlists;
        if (orderBy.isEmpty()) return getAllPlaylists();

        switch (orderBy.toLowerCase()) {
            case "nameasc":
                playlists = formatPlaylistFieldsForView(playlistsRepository.sortByNameAsc());
                return fillListOfPlaylistsIntoDtos(playlists);
            case "namedesc":
                playlists = formatPlaylistFieldsForView(playlistsRepository.sortByNameDesc());
                return fillListOfPlaylistsIntoDtos(playlists);
            case "durationasc":
                playlists = formatPlaylistFieldsForView(playlistsRepository.sortByDurationAsc());
                return fillListOfPlaylistsIntoDtos(playlists);
            case "durationdesc":
                playlists = formatPlaylistFieldsForView(playlistsRepository.sortByDurationDesc());
                return fillListOfPlaylistsIntoDtos(playlists);
            case "rankasc":
                playlists = formatPlaylistFieldsForView(playlistsRepository.sortByRankAsc());
                return fillListOfPlaylistsIntoDtos(playlists);
            case "rankdesc":
                playlists = formatPlaylistFieldsForView(playlistsRepository.sortByRankDesc());
                return fillListOfPlaylistsIntoDtos(playlists);
            default:
                throw new BAD_PARAM(String.format(INVALID_SORTING_CRITERIA, orderBy));
        }
    }

    public List<PlaylistMinDto> searchPlaylists(String keyword) {
        List<Playlist> playlists = formatPlaylistFieldsForView(playlistsRepository.searchPlaylists(keyword)); //TODO change name
        return fillListOfPlaylistsIntoDtos(playlists);
    }

    private List<Playlist> formatPlaylistFieldsForView(List<Playlist> playlists) {
        for (Playlist playlist : playlists) {
            playlist.setPlaytime(formatTotalPlaytime(playlist.getTotalPlaytime()));
            User user = usersService.getUserByID(playlist.getCreatorID()).get(0);
            playlist.setCreatorName(user.getUsername());
        }
        return playlists;
    }

    private  List<Track> getTracksFromGenreSortedByRank(Genre genre){

        return  tracksService.getTracksByGenre(genre.getId()).stream()
                .sorted(Comparator.comparingInt(Track::getRank)
                        .reversed())
                .collect(Collectors.toList());
    }

    private Set<Genre> getGenresFromGenrePercentageList(List<GenrePercentage> allGenres){
        Set<Genre> genresName = new HashSet<>();

        for (GenrePercentage genrePercentage : allGenres) {
            genresName.add(genrePercentage.getGenre());
        }
        return genresName;
    }

}
