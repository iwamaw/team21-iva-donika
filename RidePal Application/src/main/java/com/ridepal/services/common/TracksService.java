package com.ridepal.services.common;

import com.ridepal.models.Track;
import java.util.List;

public interface TracksService {
    List<Track> getAllTracks();

    List<Track> getTrackByName(String name);

    Track getTrackById(int id);

    Track createTrack(String title, String link, int duration, int deezerId, int artistId, int albumId, int rank, String previewURL);

    List<Track> getTracksByGenre(int genreID);

    String formatTotalPlaytime(int totalPlaytime);
}
