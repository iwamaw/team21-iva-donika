package com.ridepal.services.common;

import org.springframework.web.multipart.MultipartFile;
import com.ridepal.models.User;
import java.io.IOException;
import java.util.List;

public interface UsersService {
    List<User> getAllUsers();

    List<User> getUsersWithCommonUsername(String username);

    List<User> getUsersWithExactUsername(String username);

    List<User> getUserByID(int id);

    void createUser(User userToCreate);

    void deleteUser(String username);

    void updateUser(String username, User update);

    void updateUser(String username, User transferUser, MultipartFile file) throws IOException;

    List<User> getUserForSearchMethod(String username);

    boolean isUserAuthorized(String username, String authority);
}
