package com.ridepal.services;

import org.springframework.beans.factory.annotation.Autowired;
import com.ridepal.repositories.common.ArtistsRepository;
import com.ridepal.exceptions.EntityNotFoundException;
import com.ridepal.services.common.ArtistsService;
import org.springframework.stereotype.Service;
import com.ridepal.models.Artist;
import java.util.List;

import static com.ridepal.utils.Constants.*;

@Service
public class ArtistsServiceImpl implements ArtistsService {
    private ArtistsRepository artistsRepository;

    @Autowired
    public ArtistsServiceImpl(ArtistsRepository artistsRepository) {
        this.artistsRepository = artistsRepository;
    }

    @Override
    public List<Artist> getAllArtists() {
        return artistsRepository.getAllArtists();
    }

    @Override
    public List<Artist> getArtistByName(String name) {
        List<Artist> artists = artistsRepository.getArtistByName(name);
        if (artists.size() == 0) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ARTIST_BY_NAME_MESSAGE, name));
        }
        return artists;
    }

    @Override
    public Artist getArtistByID(int id) {
        Artist artist = artistsRepository.getArtistByID(id);
        if (artist == null) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ARTIST_BY_ID_MESSAGE, id));
        }
        return artist;
    }

    @Override
    public Artist getArtistByDeezerID(int deezerId) {
        if (artistsRepository.getArtistByDeezerID(deezerId).size() == 0){
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ARTIST_BY_ID_MESSAGE, deezerId)); //TODO cant make test/ separate repositories
        }
        return artistsRepository.getArtistByDeezerID(deezerId).get(0);
    }

    @Override
    public Artist createArtist(String name, int deezerId, String tracklist, int genreId, String picture) {
        return artistsRepository.createArtist(name, deezerId, tracklist, genreId, picture);
    }
}