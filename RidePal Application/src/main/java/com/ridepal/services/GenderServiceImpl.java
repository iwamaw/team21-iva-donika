package com.ridepal.services;

import org.springframework.beans.factory.annotation.Autowired;
import com.ridepal.repositories.common.GendersRepository;
import com.ridepal.exceptions.EntityNotFoundException;
import com.ridepal.services.common.GenderService;
import org.springframework.stereotype.Service;
import static com.ridepal.utils.Constants.*;
import com.ridepal.models.Gender;
import java.util.List;

@Service
public class GenderServiceImpl implements GenderService {
    private GendersRepository genderRepository;

    @Autowired
    public GenderServiceImpl(GendersRepository genderRepository) {
        this.genderRepository = genderRepository;
    }

    @Override
    public List<Gender> getAllGender() {
        return genderRepository.getAllGenders();
    }

    @Override
    public Gender getGenderByName(String name) {

        Gender gender=genderRepository.getGenderByName(name);
        if (gender == null) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_GENDER_MESSAGE, name));
        }
        return gender;
    }

    @Override
    public Gender getGenderByID(int id) {
        Gender gender=genderRepository.getGenderByID(id);
        if(gender==null){
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_GENDER_BY_ID_MESSAGE,id));
        }
        return gender;
    }
}