
    window.onload = function userModal() {
        //get open model button
        var modalBtn = document.getElementsByClassName("modalBtnUser")[0];
        //get modal element
        var modal = document.getElementById("userModal");
        //save modal element
        var saveBtn = document.getElementById("saveBtnUser");
        //get close button
        var closeBtn = document.getElementById("closeBtnUser");
        //listen for open click
        modalBtn.addEventListener('click', openModal);
        //listen for save click
        saveBtn.addEventListener('click', deleteUser);
        //listen for close click
        closeBtn.addEventListener('click', closeModal);

        function openModal() {
            modal.style.display = 'block';
        }

        function deleteUser() {
            document.getElementsByClassName("deleteUser")[0].submit();
        }

        function closeModal() {
            modal.style.display = 'none';
        }
    }