package com.ridepal.app.servicetests;

import com.ridepal.app.Factory;
import com.ridepal.exceptions.EntityNotFoundException;
import com.ridepal.models.Artist;
import com.ridepal.models.Track;
import com.ridepal.repositories.common.ArtistsRepository;
import com.ridepal.services.ArtistsServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ArtistServiceImplTests {

    @Mock
    ArtistsRepository repository;

    @InjectMocks
    ArtistsServiceImpl mockService;

    @Test
    public void getAll_Should_CallRepository() {

        List<Artist> expected = new ArrayList<>();
        expected.add(Factory.createArtist());

        when(repository.getAllArtists()).thenReturn(expected);

        List<Artist> returned = mockService.getAllArtists();

        Mockito.verify(repository, times(1)).getAllArtists();
    }

    @Test
    public void getArtistByID_shouldCallRepository_whenArtistExist() {

        Artist expected = Factory.createArtist();

        when(repository.getArtistByID(anyInt()))
                .thenReturn(expected);

        Artist returned = mockService.getArtistByID(1);

        Assert.assertSame(expected, returned);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getArtistByID_shouldThrowException_whenArtistNotExist() {

        when(repository.getArtistByID(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        Artist returned = mockService.getArtistByID(anyInt());

        Mockito.verify(repository, times(1)).getArtistByID(anyInt());
    }

//    @Test
//    public void getArtistByDeezerID_shouldCallRepository_whenArtistExist(){
//
//        List<Artist> expected=new ArrayList<>();
//        expected.add(Factory.createArtist());
//
//        when(repository.getArtistByDeezerID(anyInt()))
//                .thenReturn(expected);
//
//        Artist returned = mockService.getArtistByDeezerID(1);
//
//        Mockito.verify(repository,timeout(1)).getArtistByDeezerID(anyInt());
//    }

//    @Test(expected = EntityNotFoundException.class)
//    public void getArtistByDeezerID_shouldThrowException_whenArtistNotExist() {
//
//        when(repository.getArtistByDeezerID(anyInt()))
//                .thenThrow(new EntityNotFoundException(""));
//
//        Artist returned=mockService.getArtistByID(anyInt());
//
//        Mockito.verify(repository,times(1)).getArtistByID(anyInt());
//    }

    @Test
    public void getArtistByName_shouldCallRepository_whenArtistExist() {

        List<Artist> expected = new ArrayList<>();
        expected.add(Factory.createArtist());

        when(repository.getArtistByName(anyString()))
                .thenReturn(expected);

        List<Artist> returned = mockService.getArtistByName(anyString());

        Assert.assertSame(expected.get(0), returned.get(0));
    }

    @Test(expected = EntityNotFoundException.class)
    public void getArtistByName_shouldThrowException_whenArtistNotExist() {

        when(repository.getArtistByName(anyString()))
                .thenThrow(new EntityNotFoundException(""));

        List<Artist> returned = mockService.getArtistByName(anyString());

        Mockito.verify(repository, times(1)).getArtistByName(anyString());
    }

    @Test
    public void createArtist_shouldCallRepository() {
        Artist expected = Factory.createArtist();

        when(repository.createArtist(anyString(), anyInt(), anyString(), anyInt(), anyString()))
                .thenReturn(expected);

        Artist returned = mockService.createArtist(anyString(), anyInt(), anyString(), anyInt(), anyString());

        Mockito.verify(repository, times(1)).createArtist(anyString(), anyInt(), anyString(), anyInt(), anyString());

    }
}
