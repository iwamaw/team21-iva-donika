package com.ridepal.app.servicetests;

import com.ridepal.app.Factory;
import com.ridepal.exceptions.EntityNotFoundException;
import com.ridepal.models.Genre;
import com.ridepal.models.Playlist;
import com.ridepal.models.User;
import com.ridepal.models.dtoes.PlaylistMinDto;
import com.ridepal.repositories.UsersRepositoryImpl;
import com.ridepal.repositories.common.PlaylistsRepository;
import com.ridepal.repositories.common.UsersRepository;
import com.ridepal.services.PlaylistsServiceImpl;
import com.ridepal.services.common.GenresService;
import com.ridepal.services.common.UsersService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.omg.CORBA.BAD_PARAM;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import org.synchronoss.cloud.nio.multipart.Multipart;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PlaylistServiceImplTest {

    @Mock
    PlaylistsRepository repository;

    @Mock
    UsersService usersService;

    @Mock
    UsersRepository usersRepository;

    @Mock
    GenresService genresService;

    @InjectMocks
    PlaylistsServiceImpl mockService;


    @Test
    public void getAll_shouldCallRepository() {

        List<Playlist> test = new ArrayList<>();
        test.add(Factory.createPlaylist());
        List<User> users = new ArrayList<>();
        users.add(Factory.createUser());

        when(repository.getAllPlaylists())
                .thenReturn(test);

        when(usersService.getUserByID(anyInt())).thenReturn(users);

        mockService.getAllPlaylists();

        Mockito.verify(repository, times(1)).getAllPlaylists();
    }


    @Test
    public void getPlaylistByID_shouldCallRepository_whenPlaylistExist() {
        Playlist expected = Factory.createPlaylist();

        when(repository.getPlaylistById(anyInt()))
                .thenReturn(expected);

        Playlist returned = mockService.getPlaylistById(1);

        Assert.assertEquals(expected, returned);
    }


    @Test(expected = EntityNotFoundException.class)
    public void getPlaylistByID_shouldThrowException_whenPlaylistNotExist() {

        when(repository.getPlaylistById(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        Playlist returned = mockService.getPlaylistById(1);

        Mockito.verify(repository, times(1)).getPlaylistById(anyInt());
    }

    @Test
    public void getPrivatePlaylistByName_shouldCallRepository() {
        User user = Factory.createUser();
        Playlist playlist = Factory.createPlaylist();

        when(repository.getPrivatePlaylistByName(user.getUsername(), playlist.getId()))
                .thenReturn(playlist);

        mockService.getPrivatePlaylistByName(user.getUsername(), playlist.getId());

        Mockito.verify(repository, times(1)).getPrivatePlaylistByName(anyString(), anyInt());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getPrivatePlaylistByName_shouldThrowsException_whenPlaylistNotFound() {
        User user = Factory.createUser();
        Playlist playlist = Factory.createPlaylist();

        when(repository.getPrivatePlaylistByName(user.getUsername(), playlist.getId()))
                .thenThrow(new EntityNotFoundException(""));

        mockService.getPrivatePlaylistByName(user.getUsername(), playlist.getId());

        Mockito.verify(repository, times(1)).getPrivatePlaylistByName(anyString(), anyInt());
    }

    @Test
    public void getPlaylistByName_shouldCallRepository_whenPlaylistExist() {

        List<Playlist> expected = new ArrayList<>();
        expected.add(Factory.createPlaylist());

        when(repository.getPlaylistByName(anyString()))
                .thenReturn(expected);

        List<Playlist> returned = mockService.getPlaylistByName(anyString());

        Assert.assertEquals(expected, returned);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getPlaylistByName_shouldThrowException_whenPlaylistNotExist() {

        when(repository.getPlaylistByName(anyString()))
                .thenThrow(new EntityNotFoundException(""));

        List<Playlist> returned = mockService.getPlaylistByName(anyString());

        Mockito.verify(repository, times(1)).getPlaylistByName(anyString());

    }

    @Test
    public void getPlaylistByCreator_shouldCallRepository_whenPlaylistExist() {

        List<Playlist> expected = new ArrayList<>();
        expected.add(Factory.createPlaylist());
        List<User> users = new ArrayList<>();
        users.add(Factory.createUser());

        when(usersService.getUserByID(anyInt()))
                .thenReturn(users);

        when(repository.getPlaylistByCreator(anyInt()))
                .thenReturn(expected);

        List<Playlist> returned = mockService.getPlaylistsByCreator(1);
        Assert.assertEquals(expected, returned);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getPlaylistByCreator_shouldThrowException_whenUserNotExist() {

        when(usersService.getUserByID(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        List<Playlist> returned = mockService.getPlaylistsByCreator(1);

        Mockito.verify(repository, times(1)).getPlaylistByCreator(anyInt());
    }

    @Test
    public void updatePlaylist_shouldCallRepository() {

        Playlist expected = Factory.createPlaylist();

        when(repository.updatePlaylist(anyInt(), any(Playlist.class)))
                .thenReturn(expected);

        Playlist returned = mockService.updatePlaylist(1, expected);

        Mockito.verify(repository, times(1)).updatePlaylist(anyInt(), any(Playlist.class));
    }

    @Test
    public void deletePlaylist_shouldCallRepository() {
        Playlist expected = Factory.createPlaylist();

        when(repository.deletePlaylist(anyInt()))
                .thenReturn(expected);

        Playlist returned = mockService.deletePlaylist(1);

        Mockito.verify(repository, times(1)).deletePlaylist(anyInt());

    }

    @Test
    public void createPlaylist_shouldCallRepository() {

        Playlist expected = Factory.createPlaylist();
        List<User> users = new ArrayList<>();
        users.add(Factory.createUser());

        when(usersService.getUserByID(anyInt()))
                .thenReturn(users);

        when(repository.createPlaylist(anyString(), anyInt(), anyInt(), anySet(), anySet(), anyBoolean()))
                .thenReturn(expected);

        Playlist returned = mockService.createPlaylist(expected.getTitle(),
                expected.getTotalPlaytime(),
                expected.getCreatorID(),
                expected.getTracks(),
                expected.getGenresTags(),
                expected.isPublic());

        Mockito.verify(repository, times(1))
                .createPlaylist(anyString(), anyInt(), anyInt(), anySet(), anySet(), anyBoolean());
    }

    @Test(expected = EntityNotFoundException.class)
    public void createPlaylist_shouldThrowException_whenCreatorNotExist() {

        Playlist expected = Factory.createPlaylist();

        when(usersService.getUserByID(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        Playlist returned = mockService.createPlaylist(expected.getTitle(),
                expected.getTotalPlaytime(),
                expected.getCreatorID(),
                expected.getTracks(),
                expected.getGenresTags(),
                expected.isPublic());

        Mockito.verify(repository, times(1))
                .createPlaylist(anyString(), anyInt(), anyInt(), anySet(), anySet(), anyBoolean());

    }

    @Test
    public void updatePlaylist2_shouldCallRepository() throws IOException {
        Playlist expected = Factory.createPlaylist();

        when(repository.updatePlaylist(anyInt(), any(Playlist.class)))
                .thenReturn(expected);

        Playlist returned = mockService.updatePlaylist(1, expected);

        Mockito.verify(repository, times(1)).updatePlaylist(anyInt(), any(Playlist.class));
    }

    @Test
    public void sortByNameasc_shouldCallRepository() {

        List<Playlist> playlists = new ArrayList<>();
        playlists.add(Factory.createPlaylist());
        List<User> users = new ArrayList<>();
        users.add(Factory.createUser());

        when(repository.sortByNameAsc()).thenReturn(playlists);


        when(usersService.getUserByID(anyInt()))
                .thenReturn(users);

        mockService.sort("nameasc");

        Mockito.verify(repository, times(1)).sortByNameAsc();

    }

    @Test
    public void sortByNamedesc_shouldCallRepository() {

        List<Playlist> playlists = new ArrayList<>();
        playlists.add(Factory.createPlaylist());
        List<User> users = new ArrayList<>();
        users.add(Factory.createUser());

        when(repository.sortByNameDesc()).thenReturn(playlists);

        when(usersService.getUserByID(anyInt()))
                .thenReturn(users);

        mockService.sort("namedesc");

        Mockito.verify(repository, times(1)).sortByNameDesc();
    }

    @Test
    public void sortByDurationAsc_shouldCallRepository() {

        List<Playlist> playlists = new ArrayList<>();
        playlists.add(Factory.createPlaylist());
        List<User> users = new ArrayList<>();
        users.add(Factory.createUser());

        when(repository.sortByDurationAsc()).thenReturn(playlists);

        when(usersService.getUserByID(anyInt()))
                .thenReturn(users);

        mockService.sort("durationasc");

        Mockito.verify(repository, times(1)).sortByDurationAsc();
    }

    @Test
    public void sortByDurationDesc_shouldCallRepository() {

        List<Playlist> playlists = new ArrayList<>();
        playlists.add(Factory.createPlaylist());
        List<User> users = new ArrayList<>();
        users.add(Factory.createUser());

        when(repository.sortByDurationDesc()).thenReturn(playlists);

        when(usersService.getUserByID(anyInt()))
                .thenReturn(users);

        mockService.sort("durationdesc");

        Mockito.verify(repository, times(1)).sortByDurationDesc();
    }

    @Test
    public void sortByRankAsc_shouldCallRepository() {

        List<Playlist> playlists = new ArrayList<>();
        playlists.add(Factory.createPlaylist());
        List<User> users = new ArrayList<>();
        users.add(Factory.createUser());

        when(repository.sortByRankAsc()).thenReturn(playlists);

        when(usersService.getUserByID(anyInt()))
                .thenReturn(users);

        mockService.sort("rankasc");

        Mockito.verify(repository, times(1)).sortByRankAsc();
    }

    @Test
    public void sortByRankDesc_shouldCallRepository() {

        List<Playlist> playlists = new ArrayList<>();
        playlists.add(Factory.createPlaylist());
        List<User> users = new ArrayList<>();
        users.add(Factory.createUser());

        when(repository.sortByRankDesc()).thenReturn(playlists);

        when(usersService.getUserByID(anyInt()))
                .thenReturn(users);

        mockService.sort("rankdesc");

        Mockito.verify(repository, times(1)).sortByRankDesc();
    }

    @Test(expected = BAD_PARAM.class)
    public void sort_shouldThrowsException_whenCriteriaIsInvalid() {

        mockService.sort("test");

        Mockito.verify(repository, times(1)).sortByRankDesc();
    }

    @Test
    public void filterByGenre_shouldCallRepository() {
        List<Playlist> playlists = new ArrayList<>();
        playlists.add(Factory.createPlaylist());

        List<User> users = new ArrayList<>();
        users.add(Factory.createUser());

        Genre genre = Factory.createGenre();

        when(repository.filterByGenre(genre.getName()))
                .thenReturn(playlists);

        when(usersService.getUserByID(anyInt()))
                .thenReturn(users);

        mockService.filter("genre", genre.getName());

        Mockito.verify(repository, times(1)).filterByGenre(anyString());
    }

    @Test
    public void filterByCreator_shouldCallRepository() {
        List<Playlist> playlists = new ArrayList<>();
        playlists.add(Factory.createPlaylist());

        User user = Factory.createSecondUser();
        List<User> users = new ArrayList<>();
        users.add(Factory.createUser());

        Genre genre = Factory.createGenre();

        when(repository.filterByCreator(user.getUsername()))
                .thenReturn(playlists);

        when(usersService.getUserByID(anyInt()))
                .thenReturn(users);

        mockService.filter("creator", user.getUsername());

        Mockito.verify(repository, times(1)).filterByCreator(anyString());
    }

    @Test
    public void filterByDuration_shouldCallRepository() {
        List<Playlist> playlists = new ArrayList<>();
        playlists.add(Factory.createPlaylist());

        User user = Factory.createSecondUser();
        List<User> users = new ArrayList<>();
        users.add(Factory.createUser());

        Genre genre = Factory.createGenre();

        when(repository.filterByDuration(1, 2))
                .thenReturn(playlists);

        when(usersService.getUserByID(anyInt()))
                .thenReturn(users);

        mockService.filter("duration", "1", "2");

        Mockito.verify(repository, times(1)).filterByDuration(anyInt(), anyInt());
    }

    @Test(expected = BAD_PARAM.class)
    public void filter_shouldThrowsException_whenCriteriaIsInvalid() {

        Genre genre = Factory.createGenre();

        mockService.filter("test", "1", "2");

        Mockito.verify(repository, times(1)).filterByDuration(anyInt(), anyInt());
    }

    @Test
    public void searchPlaylists_shouldCallRepository() {
        List<Playlist> test = new ArrayList<>();
        test.add(Factory.createPlaylist());

        List<User> users = new ArrayList<>();
        users.add(Factory.createUser());

        when(repository.searchPlaylists("test"))
                .thenReturn(test);
        when(usersService.getUserByID(anyInt())).thenReturn(users);

        mockService.searchPlaylists("test");

        Mockito.verify(repository, times(1)).searchPlaylists(anyString());
    }

}
