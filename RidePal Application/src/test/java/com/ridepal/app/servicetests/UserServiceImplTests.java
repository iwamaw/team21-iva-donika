package com.ridepal.app.servicetests;

import com.ridepal.app.Factory;
import com.ridepal.exceptions.DuplicateEntityException;
import com.ridepal.exceptions.EntityNotFoundException;
import com.ridepal.models.User;
import com.ridepal.repositories.common.UsersRepository;
import com.ridepal.services.UsersServiceImpl;
import com.ridepal.services.common.GenderService;
import com.ridepal.services.common.GenresService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import java.util.ArrayList;
import java.util.List;

import static com.ridepal.app.Factory.createUser;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    @Mock
    UsersRepository repository;

    @Mock
    private UserDetailsManager detailsManager;

    @Mock
    private PasswordEncoder encoder;

    @Mock
    private GenresService genresService;

    @Mock
    private GenderService genderService;

    @InjectMocks
    UsersServiceImpl mockService;

    @Test
    public void getUser_ByCommonUsername_should_callRepository_WhenUserExist() {

        User user = createUser();
        List<User> expected = new ArrayList<>();
        expected.add(user);

        Mockito.when(repository.getUsersWithCommonUsername(anyString()))
                .thenReturn(expected);

        List<User> returned = mockService.getUsersWithCommonUsername("testUsername");

        Mockito.verify(repository, Mockito.times(1)).getUsersWithCommonUsername(anyString());

    }

    @Test
    public void getUser_ByCommonUsername_shouldReturnList_whenUserExist() {
        User user = createUser();
        List<User> expected = new ArrayList<>();
        expected.add(user);

        Mockito.when(repository.getUsersWithCommonUsername(anyString()))
                .thenReturn(expected);

        List<User> returned = mockService.getUsersWithCommonUsername("testUsername");

        Assert.assertEquals(expected.get(0), returned.get(0));

    }

    @Test(expected = EntityNotFoundException.class)
    public void getUser_ByCommonUsername_shouldThrowException_whenListIsEmpty() {

        Mockito.when(repository.getUsersWithCommonUsername(anyString()))
                .thenThrow(new EntityNotFoundException(""));

        List<User> returned = mockService.getUsersWithCommonUsername("testUsername");

        Mockito.verify(repository, Mockito.times(1)).getUsersWithCommonUsername(anyString());

    }

    @Test
    public void getAll_Should_CallRepository() {

        List<User> expected = new ArrayList<>();
        expected.add(Factory.createUser());

        Mockito.when(repository.getAllUsers()).thenReturn(expected);

        List<User> returned = mockService.getAllUsers();

        Mockito.verify(repository, times(1)).getAllUsers();
    }

//    @Test
//    @WithMockUser("iwamaw98")
//    public void createUser_shouldCallRepository_whenCreatingUser(){
//
//        User expected=Factory.createUser();
//
//        when(encoder.encode(anyString())).thenReturn(anyString());
////        doNothing().when(repository).createUser(any(User.class));
//
//        mockService.createUser(expected);
//
//
//        Mockito.verify(repository,times(1)).createUser(expected);
//    }

//    @Test(expected = DuplicateEntityException.class)
//    public void createUser_shouldThrowException_whenUsernameAlreadyExist(){
//        User expected=Factory.createUser();
//
//        Mockito.when(repository.getUserWithExactUsername(anyString()))
//                .thenThrow(new DuplicateEntityException(""));
//
//        mockService.createUser(expected);
//
//        Mockito.verify(repository,times(1)).createUser(any(User.class));
//    }

    @Test
    public void deleteUser_shouldCallRepository_whenUserExist() {

        User expected = Factory.createUser();
        List<User> users = new ArrayList<>();
        users.add(expected);

        when(repository.getUserWithExactUsername(anyString())).
                thenReturn(users);

        doNothing().when(repository).deleteUser(anyString());

        mockService.deleteUser(anyString());

        Mockito.verify(repository, times(1)).deleteUser(anyString());
    }

    @Test(expected = EntityNotFoundException.class)
    public void deleteUser_shouldThrowException_whenUsernameDoesNotExist() {
        User expected = Factory.createUser();

        Mockito.when(repository.getUserWithExactUsername(anyString()))
                .thenThrow(new EntityNotFoundException(""));

        mockService.deleteUser(expected.getUsername());

        Mockito.verify(repository, times(1)).deleteUser(anyString());
    }

    @Test
    public void updateUser_shouldCallRepository_whenUserExist() {

        User user = Factory.createUser();
        List<User> expected = new ArrayList<>();
        expected.add(user);

        when(repository.getUserWithExactUsername(anyString()))
                .thenReturn(expected);

        mockService.updateUser(user.getUsername(), user);

        Mockito.verify(repository, times(1)).updateUser(anyString(), any(User.class));
    }

    @Test(expected = EntityNotFoundException.class)
    public void updateUser_shouldThrowException_whenUserDoesNotExist() {
        User user = Factory.createUser();

        when(repository.getUserWithExactUsername(anyString()))
                .thenThrow(new EntityNotFoundException(""));

        mockService.updateUser(user.getUsername(), user);

        Mockito.verify(repository, times(1)).updateUser(anyString(), any(User.class));
    }

    @Test(expected = DuplicateEntityException.class)
    public void updateUser_shouldThrowException_whenEmailAlreadyExist() {
        User user = Factory.createUser();
        User user2 = Factory.createSecondUser();
        List<User> users = new ArrayList<>();
        users.add(user);

        when(repository.getUserWithExactUsername(user.getUsername()))
                .thenReturn(users);

        when(repository.getUserByEmail(user2.getEmail()))
                .thenThrow(new DuplicateEntityException(""));

        mockService.updateUser(user.getUsername(), user2);

        Mockito.verify(repository, times(1)).updateUser(anyString(), any(User.class));
    }

    @Test
    public void isUserAuthorized_shouldCallRepository() {
        User user = Factory.createUser();
        List<User> users = new ArrayList<>();
        users.add(Factory.createUser());

        when(repository.getUserWithExactUsername(user.getUsername()))
                .thenReturn(users);

        mockService.isUserAuthorized(user.getUsername(), "test");

        Mockito.verify(repository, times(1)).isUserAuthorized(anyString(), anyString());
    }

    @Test(expected = EntityNotFoundException.class)
    public void isUserAuthorized_shouldThrowsException_whenUserNotExist() {
        User user = Factory.createUser();

        when(repository.getUserWithExactUsername(user.getUsername()))
                .thenThrow(new EntityNotFoundException(""));

        mockService.isUserAuthorized(user.getUsername(), "test");

        Mockito.verify(repository, times(1)).isUserAuthorized(anyString(), anyString());
    }

    @Test
    public void getUserForSearchMethod_shouldCallRepository() {

        List<User> users = new ArrayList<>();
        User user = Factory.createUser();
        users.add(user);

        when(repository.getUsersWithCommonUsername(user.getUsername()))
                .thenReturn(users);

        mockService.getUserForSearchMethod(user.getUsername());

        Mockito.verify(repository, times(1)).getUsersWithCommonUsername(anyString());
    }

    @Test
    public void getUsersWithExactUsername_shouldCallRepository() {
        List<User> users = new ArrayList<>();
        User user = Factory.createUser();
        users.add(user);

        when(repository.getUserWithExactUsername(user.getUsername()))
                .thenReturn(users);

        mockService.getUsersWithExactUsername(user.getUsername());

        Mockito.verify(repository, times(1)).getUserWithExactUsername(anyString());
    }

    @Test
    public void getUserByID_shouldCallRepository() {
        List<User> users = new ArrayList<>();
        User user = Factory.createUser();
        users.add(user);

        when(repository.getUserByID(user.getID())).thenReturn(users);

        mockService.getUserByID(user.getID());

        Mockito.verify(repository, times(2)).getUserByID(anyInt());

    }


}
