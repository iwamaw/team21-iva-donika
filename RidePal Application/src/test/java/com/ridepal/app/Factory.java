package com.ridepal.app;

import com.ridepal.models.*;
import com.ridepal.models.dtoes.PlaylistMinDto;
import com.ridepal.models.dtoes.UserRegDto;
import org.hibernate.collection.internal.PersistentSet;

import java.util.HashSet;
import java.util.Set;

public class Factory {

//    public static UserRegDto createUserDTO() {
//        UserRegDto user = new UserRegDto();
//        user.setFirstName("Iva");
//        user.setLastName("Mavrodieva");
//        user.setUsername("testIvaTest");
//        user.setPassword("123456789");
//        user.setPasswordConfirmation("123456789");
//        user.setEmail("testMail@gmail.com");
//        user.setGenderID(2);
//
//        return user;
//    }

    public static User createUser() {
        Set<Playlist> playlists=new HashSet<>();
        playlists.add(createPlaylist());
        User user = new User();
        user.setID(1);
        user.setFirstName("Iva");
        user.setLastName("Mavrodieva");
        user.setUsername("testIvaTest");
        user.setPassword("123456789");
        user.setEmail("testMail@gmail.com");
        Gender gender = createGender();
        user.setGender(gender);
        user.setEnabled(true);
        user.setPlaylists(playlists);

        return user;
    }

    public static User createSecondUser(){
        Set<Playlist> playlists=new HashSet<>();
        playlists.add(createPlaylist());
        User user = new User();
        user.setID(1);
        user.setFirstName("Iva");
        user.setLastName("Mavrodieva");
        user.setUsername("testIvaTest");
        user.setPassword("123456789");
        Gender gender = createGender();
        user.setGender(gender);
        user.setEnabled(true);
        user.setPlaylists(playlists);
        return user;
    }

    public static Gender createGender() {
        Gender gender = new Gender();
        gender.setId(1);
        gender.setName("Test");

        return gender;
    }

    public static Track createTrack(){
        Track track=new Track();
        track.setId(1);
        track.setTitle("TestTrack");
        track.setDuration(123);
        track.setLink("testLink");
        track.setArtist(createArtist());
        track.setAlbum(createAlbum());
        return track;
    }

    public static Album createAlbum(){
        Album album=new Album();
        album.setID(1);
        album.setName("Album");
        album.setArtist(createArtist());
        album.setDeezerId(1);
        album.setGenreID(1);
        album.setTracklist_URL("testURL");
        album.setTracks(new HashSet<>());
        return album;
    }

    public static Artist createArtist(){
        Artist artist=new Artist();
        artist.setID(1);
        artist.setName("Artist");
        artist.setDeezerId(1);
        artist.setGenreID(1);
        artist.setAlbums(new HashSet<>());
        artist.setTracklist_URL("testURL");
        return artist;
    }

    public static Genre createGenre(){
        Genre genre=new Genre();
        genre.setId(1);
        genre.setDeezerId(1);
        genre.setName("testGenre");
        return genre;
    }

    public static Playlist createPlaylist(){
        Playlist playlist=new Playlist();
        playlist.setId(1);
        playlist.setTitle("testPlaylist");
        playlist.setTotalPlaytime(1234);
        playlist.setCreatorID(1);
        playlist.setEnabled(true);
        playlist.setGenresTags(new HashSet<>());
        playlist.setTracks(new HashSet<>());
        playlist.setPicture("testPicture");
        playlist.setPublic(false);
        return playlist;
    }

    public static PlaylistMinDto createPlaylistMinDto(){
        PlaylistMinDto playlistMinDto=new PlaylistMinDto();
        playlistMinDto.setId(1);
        playlistMinDto.setTitle("testPlaylistMinDto");
        playlistMinDto.setPicture("testPicture");
        playlistMinDto.setGenresTags(new HashSet<>());
        playlistMinDto.setCreatorName("testCreator");
        playlistMinDto.setDuration("1234");
        playlistMinDto.setRank(123);
        return playlistMinDto;
    }
}
