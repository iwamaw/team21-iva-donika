create database if not exists ride_pal;
use ride_pal;

create table gender
(
    Gender_idx int(1) not null auto_increment,
    Name varchar(6) not null,
    primary key (Gender_idx)
);

create table users
(
    ID        int(10)     not null auto_increment,
    Enabled   boolean     not null default true,
    Username  varchar(50) not null unique,
    Email     varchar(50) unique,
    Password  varchar(68) not null,
    FirstName varchar(30) ,
    LastName  varchar(30),
    Picture   longblob,
    Gender    int(1),
    BirthDate date,
    primary key (Username),
    foreign key (Gender) references gender(Gender_idx),
    index (ID),
    check (length(Username) > 3),
    check (length(Password) > 5),
    check (length(Email) > 7),
    check (length(FirstName) > 1),
    check (length(LastName) > 1)
);

create table authorities
(
    Username varchar(50) not null,
    Authority varchar(10) not null,
    unique key Username_Authority (Username, Authority),
    constraint authorities_fk foreign key (Username) references users(Username)
);

create table artists
(
    ID            int(10)      not null auto_increment,
    Name          varchar(255)  not null,
    Tracklist_URL varchar(255) not null,
    DeezerID int(10) not null default 0,
    GenreID int(10) not null default 0,
    Picture varchar(255) not null default 'N/A',
    primary key (ID)
);

create table albums
(
    ID            int(10)      not null auto_increment,
    Name          varchar(255)  not null,
    Tracklist_URL varchar(255) not null,
    ArtistID int(10) not null,
    DeezerID int(10) not null default 0,
    GenreID int(10) not null default 0,
    Picture varchar(255) not null default 'N/A',
    primary key (ID),
    foreign key (ArtistID) references artists(ID)
);

create table tracks
(
    ID int(10) not null auto_increment,
    Title varchar(255) not null,
    Link varchar(255) not null,
    Duration int not null,
    ArtistID int(10) not null,
    AlbumID int(10) not null,
    DeezerID int(10) not null default 0,
    Rank int(10) not null default 0,
    PreviewURL varchar(255),
    primary key (ID),
    foreign key (ArtistID) references artists(ID),
    foreign key (AlbumID) references albums(ID)
);

# create table albums_tracks
# (
#     Albums_Tracks_idx int(10) not null auto_increment,
#     AlbumID int(10) not null,
#     TrackID int(10) not null,
#     primary key (Albums_Tracks_idx),
#     foreign key (AlbumID) references albums(ID),
#     foreign key (TrackID) references tracks(ID)
# );

create table playlists
(
    ID int(10) not null auto_increment,
    Enabled boolean not null default true,
    Title varchar(255) not null,
    Total_Playtime int(10),
    CreatorID int(10),
    Picture longblob,
    isPublic boolean not null default false,
    Rank int(10) not null default 0,
    primary key (ID)
);

create table playlists_tracks
(
    Playlists_Tracks_idx int(10) not null auto_increment,
    PlaylistID int(10) not null,
    TrackID int(10) not null,
    primary key (Playlists_Tracks_idx),
    foreign key (PlaylistID) references playlists(ID),
    foreign key (TrackID) references tracks(ID)
);

create table genres
(
    ID int(10) not null auto_increment,
    Name varchar(20) not null,
    DeezerID int(10) not null default 0,
    primary key (ID)
);

create table playlists_genres
(
    Playlists_Genres_idx int(10) not null auto_increment,
    GenreID int(10) not null,
    PlaylistID int(10) not null,
    primary key (Playlists_Genres_idx),
    foreign key (GenreID) references genres(ID),
    foreign key (PlaylistID) references playlists(ID)
);

create table playlists_users
(
    Playlists_Genres_idx int(10) not null auto_increment,
    PlaylistID int(10) not null,
    UserID int(10) not null,
    primary key (Playlists_Genres_idx),
    foreign key (PlaylistID) references playlists(ID),
    foreign key (UserID) references users(ID)

);

# create table playlists_ratings
# (
#     Ratings_idx int(10) not null auto_increment,
#     PlaylistID int(10) not null,
#     Username varchar(50) not null,
#     Rating int(1) not null,
#     primary key (Ratings_idx),
#     foreign key (PlaylistID) references playlists(ID),
#     foreign key (Username) references users(Username),
#     constraint unique (PlaylistID, Username)
# );

# create table tracks_ratings
# (
#     Ratings_idx int(10) not null auto_increment,
#     TrackID int(10) not null,
#     Username varchar(50) not null,
#     Rating int(1) not null,
#     primary key (Ratings_idx),
#     foreign key (TrackID) references tracks(ID),
#     foreign key (Username) references users(Username),
#     constraint unique (TrackID, Username)
# );